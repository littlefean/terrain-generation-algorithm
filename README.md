# 2维世界和3维世界的地形生成算法

---

对应b站视频：

https://www.bilibili.com/video/BV1V8411n7Up/?spm_id_from=333.999.0.0

这里提供了python版本：

使用到了matplotlib第三方库，更改main函数的注释可以查看效果。

```python
def main():
    seed = 1232
    # gd = Ground2D(seed, 50)
    # gd.showPlot(100, 100, 500, 500)
    # gd = Ground2D(seed, 50)
    # gd.showPlot(100, 100, 500, 500)

    g = Ground(seed, 50)
    g.showPlot(100, 200)
    ...
```

